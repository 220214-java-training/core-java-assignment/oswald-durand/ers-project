package revature.academy;



public class Main {

    public static void main(String[] args) {
       // assignment2();
        //assignment7();
        //assignment8();
        int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        //setup my outer loop
        for (int i = 0; i < array1.length; i++) {

            //start inner loop
            for (int j = 1; j <= 10; j++) {
                int result = array1[i] * j;
                System.out.println("Result: " + result);
            }
        }
    }

    public static void assignment2() {
        int x = 5;

        if (x > 3) {
            System.out.println("The value x is greater than 3");
        } else {
            System.out.println("This value is not greater than 3");
        }

    }

    public static void assignment7() {

        for (int x = 0; x <= 10; x++) {

            if (x == 0) {
                continue;
            } else if (x % 2 == 0) {
                System.out.println(x);
            }
        }

    }
    public static void assignment8() {

        int x = 1;

       do {

                   if ( x == 1) {
                       System.out.println(x);
                   }
                   x++;

       } while (x < 10);

    }

}








