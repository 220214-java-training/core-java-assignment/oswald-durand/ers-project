package com.revature.daos;

import com.revature.models.Customer;
import com.revature.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDao{


    @Override
    public boolean create(Customer customer) {

        //use customer object to create valid SQL statement

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement("insert into customer values " +
                    "(default,?, ?, ?, ?)");){
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getEmail());
            ps.setObject(4, customer.getBirthdate());


            //provide that to SQL to JDBC in our DB
            int rowsAffected = ps.executeUpdate();  //returns the number of rows affected

            if (rowsAffected == 1) {
                return true;
            }



        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;







    }

    @Override
    public List<Customer> getAllCustomerData() {
        //create a connection to the database

        try(Connection connection = ConnectionUtil.getConnection();
            //create a statement (set params if needed)
            Statement statement = connection.createStatement();) {

            //execute statement, get a resultset in return
            ResultSet resultSet = statement.executeQuery("select * from customer");

            //creating list to store my customers when I get from DB
            List<Customer> customers = new ArrayList<>();

            //create customer objects with what we get from the database (resultset)
            // get all the fields from the records in our DB and use them to populate a customer object

            while (resultSet.next()) {

                int id = resultSet.getInt("customer_id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                LocalDate birthdate = resultSet.getObject("birthday", LocalDate.class);
                Customer c = new Customer(id,first_name, last_name, email, birthdate);
               //System.out.println(c);
                //add customer objects to a list

                customers.add(c);
            }

            return customers;


        } catch (SQLException e) {
            e.printStackTrace();
        }



        //

        return null;
    }
}
