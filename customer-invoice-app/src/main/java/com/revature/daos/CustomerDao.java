package com.revature.daos;

import com.revature.models.Customer;

import java.util.List;

public interface CustomerDao {

        /*
            CRUD operations

            - create a new customer record in db
            - read customer data
            -update customer information
            - delete customer from db
      */
        //creating a new customr
        public boolean create(Customer customer);


        //getting customer data from DB

        public List<Customer> getAllCustomerData();








}
